<img src="./src/assets/logo.png" width="100px"></img>
# Nebula Social
Nebula Social is a social application where users can share photos and thoughts about their hobbies and experiences with the rest of the world.

For more detailed information check out the [Wiki](https://gitlab.com/scalefocus-academy/students/ba1-fed/team-three-final-project/-/wikis/1.-Home)

## How to run the application
You can either visit https://nebula-social.netlify.app/ where the application is hosted or follow these steps to open it in a local server:

1. Either fork or download the application and open the folder in the cli.
2. Install all dependencies using the npm i command.
3. Start the Angular development server using the npm ng serve command. The application will be served at the default port.

## User stories
 - A user can see a public feed with posts.
 - A user can register.
 - A user can login.
 - After authorization the user can see a second private feed containing his  own posts and posts by his followers and people he follows.
 - A user can leave comments under the posts.
 - A user has access to his profile page.
 - A user can create a post by adding image, description and location.
 - A user can edit, delete and change the status of his posts.
 - A user can change his profile picture.
 - A user can request a profile deletion.
 - A user can change the default theme of application from white to dark.
 - A user can follow and unfollow other users.
 - A user can see a list with followers and the people follows.
 - A user can perform a search for other users by username.
 - A user can receive notifications by followers and followed users.

## Dependencies
  - angular-jwt
  - ngx-emoji-mart
  - ng-click-outside
  - ngx-autosize
  - ngx-socket-io
  - normalize.scss
  - socket.io-client

## Team members

 - Nikolay Siderov
 - Dimitar Tanev
